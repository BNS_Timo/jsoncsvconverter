from setuptools import setup

setup(name='Json-CSV-Converter',
      version='0.1',
      description='A tool to convert json in csv and csv in json',
      author='Timo Riedinger',
      author_email='timo.riedinger@bechtle.com',
      license='MIT',
      packages=['jsoncsvconverter'],
      zip_safe=False)