import json
import csv

'''
CSV to JSON Converter.
This code is still being revised, so there is no detailed documentation yet.
'''
class Csv2Json:

    def convert(self, csvFile):

        fileName = csvFile
        jsonName = "data2.json"

        if ".csv" in csvFile:
            jsonName = csvFile.split(".")[0] + ".json"
        elif csvFile == "":
            fileName = "data2.csv"
            jsonName = "data2.json"
        else:
            raise Exception("Not a csv file!")

        with open(fileName, 'r') as file:
            data = csv.DictReader(file)
            file.close()

        with open(jsonName, 'w') as file:
            for item in data:
                print(item)