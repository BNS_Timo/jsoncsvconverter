from csv2json import Csv2Json
from json2csv import Json2csv

'''
When the main.py is called dirctly, the query of the conversion direction occurs. 
Objects of both converters are created. In the if-query the query of the file to be converted takes place. 
Finally, the corresponding converter is called. 
If another input is made than 1 or 2 for the conversion direction, an exception occurs.
'''
def run():

    '''Initial query.'''
    type = input("1. JSON to CSV\n2. CSV to JSON\n[Insert 1 or 2]\n")

    '''Instantiation of the two objects.'''
    j2c = Json2csv()
    c2j = Csv2Json()

    '''If query for conversion direction.'''
    if type == "1":
        filename = input("Filename [data.json]: ")
        j2c.convert(filename)
    elif type == "2":
        filename = input("Filename [data2.csv]: ")
        c2j.convert(filename)
    else:
        raise Exception("Wrong conversion type")

if __name__ == "__main__":
    run()