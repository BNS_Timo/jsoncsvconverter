import json
import csv

'''
JSON to CSV Converter.
This code is still being revised, so there is no detailed documentation yet.
'''
class Json2csv:

    def convert(self, jsonFile):

        fileName = jsonFile
        csvName = "data.csv"

        if ".json" in jsonFile:
            csvName = jsonFile.split(".")[0] + ".csv"
        elif jsonFile == "":
            fileName = "data.json"
            csvName = "data.csv"
        else:
            raise Exception("Not a json file!")

        with open(fileName, 'r') as f:
            data = json.load(f)
            f.close()

        with open(csvName, 'w', newline='') as f:
            csv_file = csv.writer(f)

            for item in data:
                csv_file.writerow(item.keys())
                csv_file.writerow(item.values())
