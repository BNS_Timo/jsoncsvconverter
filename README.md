# Warning!

The conversion from CSV to JSON does not work yet!<br> 
The conversion from JSON to CSV can already be used.

# JSON-CSV-Converter

The JSON-CSV Converter is a Python tool that converts data in JSON format to CSV format and back again.<br>
This is a cli-tool. The user is asked in which direction and which file should be converted. <br>
The result is saved in a new file with the same name as the file to be converted. Only the extension is adapted.<br>

# How-To

Clone repository and run the main.py.<br>
First it asks whether CSV to JSON or JSON to CSV.<br>
This must be answered with the input 1 or 2.<br>
Then it asks for the filename of the file to convert.<br>
The file should be in the same directory.<br>