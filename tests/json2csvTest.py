import os
import unittest
from time import sleep

from json2csv import Json2csv


class MyTestCase(unittest.TestCase):

    def setUp(self):
        json2csv = Json2csv()
        with open('test.json', 'w') as file:
            file.write("[{\"id\":2,\"networkId\":\"L_671036344478209806\",\"name\":\"KDW_Future-Use.2\",\"applianceIp\":\"10.2.1.254\",\"subnet\":\"10.2.1.0/24\"}]")
        print("Datei erzeugt!")

    def tearDown(self):
        os.remove("test.json")
        print("Datei gelöscht!")

    def test_test(self):
        print("Test!")
        sleep(20)


if __name__ == '__main__':
    unittest.main()
