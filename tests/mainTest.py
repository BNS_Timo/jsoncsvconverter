import unittest
from unittest import mock
import main


class MyTestCase(unittest.TestCase):

    def test_wrong_convertion_user_input(self):
        original_input = mock.builtins.input
        mock.builtins.input = lambda _: "3"
        with self.assertRaises(Exception):
            main.run()
        mock.builtins.input = original_input

    '''
    def test_wrong_file_for_json_to_csv_user_input(self):
        original_input = mock.builtins.input
        mock.builtins.input = lambda _: "1"
        self.assertEqual()

        mock.builtins.input = original_input
    '''

if __name__ == '__main__':
    unittest.main()
